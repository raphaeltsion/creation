// this specific url appears to change it's own title over the course of the content loading
// a timeout of 5 seconds seemed to suffice to achieve the desired effect 
setTimeout(() => {
  document.title = 'Creation Demo Application';
} , 7000);
